package org.gjt.sp.jedit.cpi220.dataStructures;

import java.util.NoSuchElementException;
import java.util.Iterator;

import org.gjt.sp.util.Log;
import org.gjt.sp.jedit.cpi220.performance.SpaceCalculator;


public class ResizingArrayStack<Item> implements Iterable<Item> 
{
    private Item[] a;         // array of items
    private int N;            // number of elements on stack


    /**
     * Initializes an empty stack.
     */
    public ResizingArrayStack() {
        a = (Item[]) new Object[2];
    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.IStack#isEmpty()
	 */
	public boolean isEmpty() {
        return N == 0;
    }

    /**
     * Returns the number of items in the stack.
     * @return the number of items in the stack
     */
    public int size() {
        return N;
    }


    // resize the underlying array holding the elements
    private void resize(int capacity) {
        assert capacity >= N;
        Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < N; i++) {
            temp[i] = a[i];
        }
        a = temp;
    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.IStack#push(Item)
	 */
	public void push(Item item) {
    	if (N == a.length) resize(2*a.length);    // double size of array if necessary
        a[N++] = item;    // add item
       

    }

    /* (non-Javadoc)
	 * @see org.gjt.sp.jedit.cpi220.IStack#pop()
	 */
	public Item pop() {
        if (isEmpty()) return null;
        Item item = a[N-1];
        a[N-1] = null;                              // to avoid loitering
        N--;
        // shrink size of array if necessary
        if (N > 0 && N == a.length/4) resize(a.length/2);
        return item;
    }


    /**
     * Returns (but does not remove) the item most recently added to this stack.
     * @return the item most recently added to this stack
     * @throws java.util.NoSuchElementException if this stack is empty
     */
    public Item peek() {
        if (isEmpty()) return null;
        return a[N-1];
    }
    
    /**
     * Returns an iterator to this stack that iterates through the items in LIFO order.
     * @return an iterator to this stack that iterates through the items in LIFO order.
     */
    public Iterator<Item> iterator() {
        return new ResizingArrayStackIterator();
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ResizingArrayStackIterator implements Iterator<Item> {
        private int i;

        public ResizingArrayStackIterator() {
            i = N-1;
        }

        public boolean hasNext() {
            return i > 0;
        }

        public void remove() {
            for (int j = i; j < N-1; j++)
            	a[j] = a[j+1];
            pop();
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            return a[i--];
        }
    }


}






